package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continueChoices = Arrays.asList("y", "n");

    Random random = new Random();
    
    public void run() {
        boolean continuePlaying = true;
        
    	while (continuePlaying) {
    		System.out.println("Let's play round "+roundCounter);
    	
    		String humanChoice = getHumanInput();
    		String computerChoice = computerChoice();
    	
    	
    		System.out.print("Human chose "+humanChoice);
    		System.out.print(", computer chose "+computerChoice);
    		
    		if (isWinner(computerChoice, humanChoice)) {
    			System.out.println(". Computer wins!");
    			computerScore++;
    		} else if (isWinner(humanChoice, computerChoice) ){
    			System.out.println(". Human wins!");
    			humanScore++;
    		} else {
    			System.out.println(". It's a tie!");
    		}
    		
    		System.out.println("Score: human "+humanScore +"," +" computer "+computerScore);
    	
    		
    		continuePlaying = continuePlaying();
    		roundCounter++;
    	}
    	System.out.println("Bye bye :)");
    }
    
    public String getHumanInput() {
    	boolean understood = false;
    	String humanChoice = null;
    	while (understood == false) {
    		humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
    		if (validateInput(humanChoice, rpsChoices)) {
    			understood = true;
    		} else {
    			System.out.println("I do not understand "+humanChoice +"."+ " Could you try again?");

    		}
    	} 
    	return humanChoice;
    }
    
    public boolean continuePlaying() {
    	
    	boolean understood = false;
    	boolean continuePlay = false;
    	while (understood == false) {
    		
    		String playMore = readInput("Do you wish to continue playing? (y/n)?");
    		if (validateInput(playMore, continueChoices)) {
    			understood = true;
    			if (playMore.toLowerCase().equals("y")) {
    				continuePlay = true;
    			}
    		} else {
    			System.out.println("I do not understand "+playMore +"."+ " Could you try again?");
    		}
    	}
		return continuePlay;
    }
    
    public boolean validateInput(String input, List<String> validInput) {
    	return validInput.contains(input.toLowerCase());
    }
    
    public boolean isWinner(String choice1, String choice2) {
    	if (choice1.equals("paper")) return choice2.equals("rock");
    	else if (choice1.equals("scissors")) return choice2.equals("paper");
    	else return choice2.equals("scissors");
    }
    
    public String computerChoice() {
    	int index = random.nextInt(rpsChoices.size());
		return rpsChoices.get(index);
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
